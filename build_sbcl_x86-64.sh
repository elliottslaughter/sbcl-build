#!/bin/bash

# Download MinGW-w64 from:
# http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/rubenvb/gcc-4.7-release/x86_64-w64-mingw32-gcc-4.7.2-release-win64_rubenvb.7z/download

mingw64_dir="/c/Bin/MinGW64_rubenvb"
export PATH="$mingw64_dir/bin:/usr/local/bin:/bin:/c/Program Files (x86)/Git/bin:$SBCL_HOME"
SBCL_ARCH=x86-64 sh make.sh &> build.txt
tail -n 20 build.txt
